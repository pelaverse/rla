# RLA <img src="man/figures/RLA_logo_3.png" align="right" width="120" />

<br>

## Installation

``` r
install.packages("remotes")
remotes::install_gitlab(host = "https://gitlab.univ-lr.fr", repo = "pelaverse/rla")
library(RLA)
```

## TO DO

- [ ] Add data on common dolphins in the Bay of Biscay

``` r
Project / data (life history) 
        / stan (stan models) 
        / doc (figures, simulations vignettes, ...) 
```        

- [x] Put stan models in data of {RLA}, to have them in "/model" when creating a R project 
- [x] Bonus : hex sticker (draft in man/figures/logo_RLA.svg) :art:
- [ ] Make "simulations" vignette work with new code updates
- [x] check of the package with no errors
- [ ] check of the package with no warnings
- [ ] check of the package with no notes
- [x] Shiny for benchmarking control rules (https://pelabox.univ-lr.fr/pelagis/DART/)
- [ ] Define and use more classes to cleanly separate **operating model** objects and **control rule** objects
