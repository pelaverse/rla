<!-- NEWS.md is maintained by https://cynkra.github.io/fledge, do not edit -->

# RLA 0.2.0

- add **stan** code for the stochastic Surplus Production Model described in Ouzoulias (2022)
- rename `pellatomlinson_pbr()` to `pellatomlinson_agg()` to separate clearly the operating model
from the control rule. The suffix agg stands for an age-aggregrated operating model.
- rename `pellatomlinson_rla()` to `pellatomlinson_dis()` to separate clearly the operating model
from the control rule. The suffix dis stands for an age-disaggregrated operating model.
- rename `vital_param_agg()` for use with `pellatomlinson_agg()`
- rename `vital_param_dis()` for use with `pellatomlinson_dis()`
- all test modified and run correctly, examples of all function adapted
* vignettes to be updated (are currently removed)

# RLA 0.1.6.9001

- modify `vital_param_args()` into vital_param_pbr() for **PBR** and add `vital_param_rla()` for **RLA** and **PBR_nouveau**
- all test modified and run correctly, examples of all function adapted
* In `pellatomlinson_pbr()` convert `vital_param` into a function to ease its use
* `pellatomlinson_pbr()` add `vital_parm` that gather MNPL, Rmax, K and z
* vignettes old update (vignettes need to be simplified to run)


# RLA 0.1.6.9000

* `citation("RLA")`updated with RLA paper in Frontiers
* New `depensation(decay, x)` to implement the probability of a female to meet a male.
  `pbr_nouveau()` and `pellatomlinson_rla()`now have `Allee` argument which is used in `depensation()` :
  
  ```r
  # In pellatomlinson_rla() 
  hookup <- depensation(decay = allee, x = sum(Nm[tt, -1]) / Km)
  ```


# RLA 0.1.6

* Added a `NEWS.md` file to track changes to the package.
