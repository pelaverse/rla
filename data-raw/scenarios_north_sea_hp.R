## code to prepare `scenarios_north_sea_hp` dataset goes here
lapply(c("tidyverse", "RLA"),
       library, character.only = TRUE
       )

data("north_sea_hp")

set.seed(20230106)
n_sim <- 1e3
param <- data.frame(seed = sample.int(1e6, size = n_sim, replace = FALSE),
                    MNPL = rtnorm(n_sim, location = 0.6, scale = 0.025, left = 0.5, right = 0.7),
                    cv_byc = round(runif(n_sim, 0.05, 0.50), 3),
                    cv_env = round(runif(n_sim, 0.00, 0.20), 3),
                    cv_obs = round(runif(n_sim, 0.15, 0.30), 3),
                    MNP = rgamma(n_sim, shape = 3.645, rate = 1 / (1.094E-2)),
                    rate = round(runif(n_sim, 0.001, 0.050), 3)
                    ) %>%
  mutate(MNP = 1 + MNP)

system.time(
  rla <- lapply(1:nrow(param),
                function(i) {
                  pellatomlinson_dis(
                    vital_param = vital_param_dis(
                      L = north_sea_hp$life_history$L,# maximum longevity,
                      K = north_sea_hp$life_history$K, # carrying capacity
                      # vulnerabilities of bycatch
                      eta = north_sea_hp$life_history$eta,
                      # survival probabilities
                      phi = north_sea_hp$life_history$phi,
                      # proportions of mature females in each age class
                      m = north_sea_hp$life_history$maturity,
                      MNP = param$MNP[i],
                      MNPL = param$MNPL[i]
                    ),
                    # series of removals
                    removals = bycatch_rw(n = 51,
                                          K = north_sea_hp$life_history$K,
                                          rate = param$rate[i],
                                          cv = param$cv_byc[i],
                                          seed_id = param$seed[i]
                                          ),
                    CV = param$cv_obs[i],
                    # CV of environmental stochasticity on birth rate
                    CV_env = param$cv_env[i],
                    # scans survey
                    scans = c(29, 40, 51),
                    verbose = FALSE,
                    # for reproducibility purposes
                    seed_id = param$seed[i]
                    )
                  }
                )
  )

### identify scenario with depleted population
scenarios_north_sea_hp <- param %>%
  mutate(current_depletion = do.call('c',
                                     lapply(rla,
                                            function(l) {
                                              l$depletion[length(l$depletion)]
                                            })
                                     ),
         sim = 1:n()
         ) %>%
  filter(current_depletion >= 0.3,
         current_depletion < MNPL
         ) %>%
  mutate(level_mnp = cut(MNP, breaks = seq(1.0, 1.1, 0.01)),
         level_mnpl = cut(MNPL, breaks = seq(0.3, 0.7, 0.1)),
         level_depletion = cut(current_depletion, breaks = seq(0.3, 0.7, 0.1))
         ) %>%
  slice_sample(n = 1e2)

rm(list = ls()[-match("scenarios_north_sea_hp", table = ls())])

usethis::use_data(scenarios_north_sea_hp, overwrite = TRUE)
