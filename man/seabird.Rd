% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/seabird.R
\name{seabird}
\alias{seabird}
\title{A function to simulate a population dynamics with a stage structured operating model
with a Pella-Tomlinson functional form for the density-dependent process on fledling success rate.}
\usage{
seabird(
  burnin = 500,
  vital_param = vital_param_seabird(),
  removals,
  p_det = 0.9,
  census = "pairs",
  CV_env = 0,
  rho = 0.5,
  scans = NULL,
  everything = FALSE,
  verbose = TRUE,
  seed_id = NULL,
  random = FALSE
)
}
\arguments{
\item{burnin}{a count scalar. The number of time steps to run the Pella-Tomlison
process without anthropogenic removals. Default to 500.}

\item{vital_param}{argument have to be included in the function \code{vital_param_seabird()}
\itemize{
\item \strong{Nf} number of females in each age class. Default to NULL and to be used to
bypass burn-in.
\item \strong{Nm} number of males in each age class. Default to NULL and to be used to
bypass burn-in.
\item \strong{depletion0} positive scale; depletion at the beginning of the
population trajectory. Ignored if Nf and Nm are given. Default to 30\\%.
\item \strong{MNPL} a proportion; the maximum net productivity level as fraction of
carrying capacity. Default to 0.6.
\item \strong{z} a positive scalar (internal). Default to NULL.
\item \strong{K} a count scalar; carrying capacity, default to 1E4
\item \strong{eta} a count vector; relative vulnerabilities of each stage to removals.
Must be of length 4.
\item \strong{sexbias} a number vector of length 2 with female-male relative vulnerabilities
to removals.
\item \strong{phi} a vector of 4 probabilities; stage-specific survival probabilities for
fleglings, juveniles, adult breeders and adult non-breeders.
\item \strong{afr} a positive scalar; age at first reproduction of females and males.
\item \strong{breed} a probability: the average breeding probability.
\item \strong{eggs} a vector with three args. to specify (i) the average number of eggs (positive scalar),
(ii) a count scalar for right truncation (set to NULL for no truncation); and
(iii) the dispersion (ratio of variance to mean).
\item \strong{hatch} a probability: the average hatching probability.
\item \strong{sexratio} a number vector of length 2 with female-male sex-ratio at birth.
\item \strong{MNP} a positive scalar greater than 1 controlling the maximum net productivity.
Default to using the allometric equation from Niel and Lebreton (2005).
\item \strong{f_max} a positive scalar (internal). The maximum fledging rate. Default to NULL.
\item \strong{f_K} a positive scalar (internal). The equilibrium fledging rate. Default to NULL.
\item \strong{K_ad} a positive scalar (internal). Adult carrying capacity. Default to NULL.
\item \strong{juv2ad} a probability (internal): transition probability from juvenile to adult state. Default to NULL.
}}

\item{removals}{a count vector; time-series of anthropogenic removals of seabirds.}

\item{p_det}{a probability; expected detection probability for the survey estimates.
Default to 0.9.}

\item{census}{character; either 'pairs' or 'fledglings' for a survey of
breeding pairs or a count of fledglings. The data-generating mechanism is a binomial
process, implying that the survey estimate is lower than or equal to the true count}

\item{CV_env}{a positive scalar; coefficient of variation for environmental stochasticity.
Default to 0.}

\item{rho}{a scalar between -1 and 1; correlation parameter of the correlated
random walk model for environmental stochasticity. Default to 0.5.}

\item{scans}{a count vector; years when surveys are taking place}

\item{everything}{logical; do you want an output with full information from the simulation?
Default to FALSE.}

\item{verbose}{logical; print some messages. Default to TRUE.}

\item{seed_id}{a count scalar, seed for reproducibility purposes.}

\item{random}{logical; use another seed than the one provided? Useful to continue
a simulation. Default to FALSE.}
}
\value{
a list with:
\enumerate{
\item N : a count matrix with simulated population abundance in each age class.
\item male : a count matrix with simulated male abundance in each age class.
\item female : a count matrix with simulated female abundance in each age class.
\item productivity : a matrix with average female productivity for each age class.
\item abundance : a count vector with the total simulated population abundance.
\item survey : a dataframe with 3 columns summarizing simulated survey estimates
\item removals : a count vector of anthropogenic removals (same as input removals)
\item depletion : a numeric vector of depletion level (equals to N/K)
excluding newborns.
\item Nf : last row of matrix female above; useful to restart simulation in a new call.
\item Nm : last row of matrix male above; useful to restart simulation in a new call.
\item fledgling_rate : a vector with the average fledgling rate.
\item z : a positive scalar, the exponent of the Pella-Tomlinson process.
\item MNPL : the Maximum Net Productivity Level (same as input MNPL).
\item K : a count, the carrying capacity (same as input).
\item eta : same as input.
\item sexbias : same as input.
\item phi : same as input.
\item afr : same as input.
\item breed : same as input.
\item eggs : same as input.
\item hatch : same as input.
\item sexratio : same as input.
\item MNP : same as input.
\item f_max : maximum fledgling rate.
\item f_K : fledgling rate at equilibrium.
\item p_det : same as input.
\item census : same as input.
\item CV_env : a positive scalar, the coefficient of variation for environmental
stochasticity (same as input CV_env).
\item rho : a scalar (correlation), autocorrelation in environmental stochasticity
(same as input rho).
\item seed_id : a count scalar, the seed used in the simulation
(same as input seed_id, if provided).
}
}
\description{
A function to simulate a population dynamics with a stage structured operating model
with a Pella-Tomlinson functional form for the density-dependent process on fledling success rate.
}
\details{
Simulations are carried out with the following steps:
1 - anthropogenic mortality takes place: all stages are affected
2 - seabirds age and die of natural causes
3 - seabirds reproduce
4 - increment time by one unit and repeat 1-3
Stochastic processes are used throughout so demographic stochasticity can be
investigated by using a small value for K for example.
}
\examples{
\dontrun{
set.seed(123)
### series of removals
kittiwake_removals = ceiling(runif(30, 5e1, 5e2))
### complete output
system.time(
  kitti <- seabird(
    vital_param = vital_param_seabird(
      # survival probabilities
      phi = c(0.65, 0.70, 0.90, 0.75),
      afr = 4,
      breed = 0.9,
      eggs = c(1.6, 2, 1),
      hatch = 0.95
    ),
    # series of removals
    removals = kittiwake_removals,
    # CV of environmental stochasticity on fledgling rate
    CV_env = 0.1,
    # for complete output
    everything = TRUE,
    # for reproducibility purposes,
    seed_id = 20201220
  )
)

plot(kitti$depletion, las = 1, type = 'l', xlab = "# years", ylab = "depletion",
     bty = 'n', ylim = c(0, 1.25)
     )
abline(h = c(1, 1.25), col = "tomato", lty = 1) # carrying capacity

plot(kitti$male[,1], las = 1)
plot(kitti$female[,1], las = 1)

### abundance : survey estimates and truth
plot(kitti$survey$mean, las = 1, type = 'l',
     xlab = "# years", ylab = "Abundance", bty = 'n',
     col = "midnightblue"
     )
lines(kitti$abundance[501:530], lty = 2, col = "tomato", lwd = 2) # truth
abline(h = 1e4, col = "seagreen", lty = 1) # carrying capacity

### transversal slice
plot(kitti$N[1, ], las = 1, type = 'l', xlab = "Stage",
     ylab = quote(N[1]), bty = 'n'
     )
plot(kitti$N[100, ], las = 1, type = 'l', xlab = "Stage",
     ylab = quote(N[100]), bty = 'n'
     )
plot(kitti$N[500, ], las = 1, type = 'l', xlab = "Stage",
     ylab = quote(N[500]), bty = 'n'
     )
plot(kitti$N[530, ], las = 1, type = 'l', xlab = "Stage",
     ylab = quote(N[530]), bty = 'n'
     )

### check the rest
plot(kitti$N[, 1], las = 1, type = 'l', xlab = "# years",
     ylab = "0 years old", bty = 'n'
     )
plot(kitti$N[, 2], las = 1, type = 'l', xlab = "# years",
     ylab = "Juveniles", bty = 'n'
     )
plot(kitti$N[, 3], las = 1, type = 'l', xlab = "# years",
     ylab = "Breeders", bty = 'n'
     )
plot(kitti$N[, 4], las = 1, type = 'l', xlab = "# years",
     ylab = "Non-Breeders", bty = 'n'
     )
}
}
\seealso{
\code{\link{lambda_a_la_cole}}, \code{\link{niel_lebreton}},
\code{\link{pbr_nouveau}}
}
