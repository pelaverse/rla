#' Summary plots of simulated population dynamics
#'
#' Visualize graphical and tabular summaries of Pella-Tomlinson population dynamics
#'
#' @param op_model output of one of \code{\link{pellatomlinson_dis}} or
#' \code{\link{pellatomlinson_agg}}.
#' @param lower_zero whether to start y axis of each plot by 0 or let ggplot
#' adjust with values.
#' @param ... other optional args.
#'
#' @return a plot of class patchwork (that is a collage of ggplots)
#'
#' @seealso \code{\link{pellatomlinson_dis}}, \code{\link{pellatomlinson_agg}}
#'
#' @export
#'
#' @import patchwork ggplot2 dplyr ggtext
#' @importFrom assertthat assert_that %has_name%
#' @importFrom gridExtra tableGrob
#' @importFrom stats reshape
#'
#' @examples
#' \dontrun{
#'   set.seed(123)
#'   ### series of catches
#'   hp_catches = ceiling(seq(3000,5000,4))
#'   ### complete output
#'   hp <- pellatomlinson_dis(
#'     vital_param = vital_param_dis(L = 22,# maximum longevity
#'                                   # vulnerabilities of bycatch
#'                                   eta = c(2, 2, rep(1, 21)),
#'                                   # survival probabilities
#'                                   phi = c(0.85, 0.87, rep(0.91, 20), 0.0),
#'                                   # proportions of mature females in each age class
#'                                   m = c(0, 1 / (1 + exp( (4 - 0:21) / 0.5)))
#'                                   ),
#'     # series of catches
#'     catches = hp_catches,
#'     # CV of environmental stochasticity on birth rate
#'     CV_env = 0,
#'     # for complete output
#'     everything = TRUE,
#'     # for reproducibility purposes,
#'     seed_id = 20201220,
#'     rho = 0.99
#'   )
#'   summary_plot(hp, lower_zero = TRUE)
#' }
summary_plot <- function(op_model, lower_zero = FALSE, ...) {
  UseMethod("summary_plot")
}

#' @export
summary_plot.pellatomlinson_dis <- function(op_model, lower_zero = FALSE, ...) {

  assert_that(is.list(op_model))
  assert_that(is.logical(lower_zero))
  assert_that(op_model %has_name% c("abundance","removals","survey","CV","depletion","productivity","birthrate"))



  param_long <- c("Abundance","Depletion","Birthrate","Productivity","Removals")
  length_tot <- length(op_model$abundance)
  length_burnin <- length_tot - length(op_model$removals)

  # df removals
  df_survey_estimates <- data.frame(
    time =  (length_burnin + 1) : length_tot,
    removals = op_model$removals
  )

  df_survey <- data.frame(
    time = length_burnin + op_model$survey$scans,
    survey_estimate = op_model$survey$mean
  )

  df_survey_estimates <- df_survey_estimates %>%
    left_join(df_survey, by = "time") %>%
    select(survey_estimate, time, removals)


  # build projection background
  time_rect <- data.frame(
    proj_min = length_burnin + 1,
    proj_max = Inf,
    ymin = -Inf,
    ymax = Inf
  )

  tmp_df <- c("abundance","depletion","birthrate","productivity") %>%
    lapply(function(x)getElement(op_model,x)) %>%
    do.call(cbind, .) %>%
    as.data.frame() %>%
    mutate(time = 1:n()) %>%
    left_join(df_survey_estimates, by = "time")


  colnames(tmp_df)[c(1:4,ncol(tmp_df))] <- param_long


  # color associated to param
  length_param <- length(c(param_long, "survey_estimate"))
  param_col <- c("#4DAF4A", "#377EB8", "#FF7F00","#984EA3","#E41A1C", "#edae49", "#F781BF", "#FFFF33")[1:length_param]
  names(param_col) <- c(param_long, "survey_estimate")


  sigma <- sqrt(log1p(op_model$CV * op_model$CV))
  mu <- log(op_model$abundance) - 0.5 * sigma * sigma
  q10 <- qlnorm(0.1, meanlog = mu, sdlog = sigma)
  q90 <- qlnorm(0.9, meanlog = mu, sdlog = sigma)



  tmp_df <- tmp_df %>%
    mutate(q10_abundance = q10,
           q90_abundance = q90) %>%
    select(time, everything())


  # Modify birthrate value with sex-ratio value (birthrate * 1/proba_female)
  proba_female <- op_model$sexratio[1] / sum(op_model$sexratio)
  tmp_df <- tmp_df %>%
    mutate(Birthrate = Birthrate * 1/proba_female) %>%
    mutate(Birthrate = ifelse(Birthrate >= 1, 1, Birthrate))


  # Abundance
  g_abundance <- tmp_df %>%
    select(Abundance, time, q10_abundance, q90_abundance) %>%
    ggplot() +
    geom_rect(data = time_rect,
              aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
              fill = alpha("grey", 0.3),
              inherit.aes = FALSE) +
    geom_line(aes(y = Abundance, x = time, colour = "Abundance")) +
    geom_ribbon(aes(y = Abundance, ymin = q10_abundance, ymax = q90_abundance, x=time), fill = alpha(param_col["Abundance"],0.2)) +
    scale_colour_manual(values = param_col) +
    geom_hline(yintercept = op_model$K, linetype = "dashed") +
    labs(subtitle = "Abundance") +
    theme_bw() +
    theme(axis.title.x=element_blank(),
          axis.text.x=element_blank(),
          axis.ticks.x=element_blank(),
          axis.title.y=element_blank(),
          legend.position="none")


  # Depletion
  g_depletion <- tmp_df %>%
    select(Depletion, time) %>%
    ggplot(aes(y = Depletion, x = time, colour = "Depletion")) +
    geom_rect(data = time_rect,
              aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
              fill = alpha("grey", 0.3),
              inherit.aes = FALSE) +
    geom_line() +
    scale_colour_manual(values = param_col) +
    labs(subtitle = "Depletion") +
    theme_bw() +
    theme(axis.title.y=element_blank(),
          legend.position="none")


  # Birthrate
  g_birthrate <- tmp_df %>%
    select(Birthrate, time) %>%
    ggplot(aes(y = Birthrate, x = time, colour = "Birthrate")) +
    geom_rect(data = time_rect,
              aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
              fill = alpha("grey", 0.3),
              inherit.aes = FALSE) +
    geom_line() +
    scale_colour_manual(values = param_col) +
    labs(subtitle = expression(paste("Birthrate x ",frac(1,'proba_female'),sep=""))) +
    theme_bw() +
    theme(axis.title.y=element_blank(),
          legend.position="none")

  # Productivity
  g_productivity <- tmp_df %>%
    select(Productivity,time) %>%
    ggplot(aes(y = Productivity, x = time, colour = "Productivity")) +
    geom_rect(data = time_rect,
              aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
              fill = alpha("grey", 0.3),
              inherit.aes = FALSE) +
    geom_line() +
    scale_colour_manual(values = param_col) +
    labs(subtitle = "Productivity (female calves)") +
    theme_bw() +
    theme(axis.title.y=element_blank(),
          legend.position="none",
          axis.title.x=element_blank(),
          axis.text.x=element_blank(),
          axis.ticks.x=element_blank())


  # projection
  g_proj <- tmp_df %>%
    filter(time > length_burnin) %>%
    select(time, Abundance, q10_abundance, q90_abundance, survey_estimate) %>%
    ggplot() +
    geom_rect(data = time_rect,
              aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
              fill = alpha("grey", 0.3),
              inherit.aes = FALSE) +
    geom_ribbon(aes(y = Abundance, ymin = q10_abundance, ymax = q90_abundance, x=time), fill = alpha(param_col["Abundance"],0.2)) +
    geom_point(aes(y = survey_estimate, x = time, colour = "survey_estimate"), size = 2, na.rm = TRUE) +
    geom_line(aes(y = Abundance, x = time, colour = "Abundance")) +
    scale_colour_manual(values = param_col) +
    geom_hline(yintercept = op_model$K, linetype = "dashed") +
    labs(subtitle = paste0("Projected abundance<br>",
                           "(<span style='color:",param_col["Abundance"],";'>**real**</span> vs ",
                           "<span style='color:",param_col["survey_estimate"],";'>**survey estimated**</span>)")) +
    theme_bw() +
    theme(plot.subtitle = element_markdown(lineheight = 1.1),
          legend.text = element_markdown(size = 11),
          legend.position="none",
          axis.title.y=element_blank(),
          axis.title.x=element_blank(),
          axis.text.x=element_blank(),
          axis.ticks.x=element_blank())


  # removals
  g_removals <- tmp_df %>%
    filter(time > length_burnin) %>%
    select(time, Removals) %>%
    ggplot() +
    geom_rect(data = time_rect,
              aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
              fill = alpha("grey", 0.3),
              inherit.aes = FALSE) +
    geom_point(aes(y = Removals, x = time, colour = "Removals"), size = 2, na.rm = TRUE) +
    geom_line(aes(y = Removals, x = time, colour = "Removals"), na.rm = TRUE) +
    scale_colour_manual(values = param_col) +
    labs(subtitle = "Removals") +
    theme_bw() +
    theme(axis.title.y=element_blank(),
          legend.position="none")


  # build table
  df_table <- data.frame(
    z = op_model$z,
    MNPL = op_model$MNPL,
    K = op_model$K,
    L = op_model$L,
    MNP = op_model$MNP,
    b_max = op_model$b_max,
    b_K = op_model$b_K,
    CV = op_model$CV,
    CV_env = op_model$CV_env,
    rho = op_model$rho,
    seed_id = op_model$seed_id
    ) %>%
    mutate(across(starts_with("b_"), ~round(.x, digits = 3))) %>%
    reshape(times = names(.),
            v.names = "value",
            direction = "long",
            varying = 1:ncol(.),
            timevar = "param",
            new.row.names = NULL
            ) %>%
    select(-id) %>%
    mutate(value = as.character(value))

  rownames(df_table) <- NULL

  tmp_table <- df_table %>%
    gridExtra::tableGrob(cols = NULL, rows = NULL)

  # assemble all plot with patchwork

  design <- "
  abcd
  aefg
  "

  if(lower_zero == TRUE) {

    g_long_time <- wrap_elements(tmp_table) +
      g_productivity + ylim(0,NA) +
      g_abundance + ylim(0,NA) +
      g_proj + ylim(0,NA) +
      g_birthrate + ylim(0,NA) +
      g_depletion + ylim(0,NA) +
      g_removals + ylim(0,NA) +
      plot_layout(design = design, widths = c(1.0,1.25,1.25,1.25))

  } else {

    g_long_time <- wrap_elements(tmp_table) +
      g_productivity +
      g_abundance +
      g_proj +
      g_birthrate +
      g_depletion +
      g_removals +
      plot_layout(design = design, widths = c(1.0,1.25,1.25,1.25))

  }

  return(g_long_time)

}


#' @export
summary_plot.pellatomlinson_agg <- function(op_model, lower_zero = FALSE, ...) {

  assert_that(is.list(op_model))
  assert_that(is.logical(lower_zero))
  assert_that(op_model %has_name% c("N","removals","survey","CV","depletion"))

  param_long <- c("Abundance","Depletion","Removals")
  length_tot <- length(op_model$N)
  length_burnin <- length_tot - length(op_model$removals)

  # df removals
  df_survey_estimates <- data.frame(
    time =  (length_burnin + 1) : length_tot,
    removals = op_model$removals
  )

  df_survey <- data.frame(
    time = length_burnin + op_model$survey$scans,
    survey_estimate = op_model$survey$mean
  )

  df_survey_estimates <- df_survey_estimates %>%
    left_join(df_survey, by = "time") %>%
    select(survey_estimate, time, removals)

  # build projection background
  time_rect <- data.frame(
    proj_min = length_burnin + 1,
    proj_max = Inf,
    ymin = -Inf,
    ymax = Inf
  )

  tmp_df <- c("N","depletion") %>%
    lapply(function(x)getElement(op_model,x)) %>%
    do.call(cbind, .) %>%
    as.data.frame() %>%
    mutate(time = 1:n()) %>%
    left_join(df_survey_estimates, by = "time")


  colnames(tmp_df)[c(1:2,ncol(tmp_df))] <- param_long


  # color associated to param
  length_param <- length(c(param_long, "survey_estimate"))
  param_col <- c("#4DAF4A", "#377EB8", "#E41A1C", "#edae49", "#F781BF", "#FFFF33")[1:length_param]
  names(param_col) <- c(param_long, "survey_estimate")


  sigma <- sqrt(log1p(op_model$CV * op_model$CV))
  mu <- log(op_model$N) - 0.5 * sigma * sigma
  q10 <- qlnorm(0.1, meanlog = mu, sdlog = sigma)
  q90 <- qlnorm(0.9, meanlog = mu, sdlog = sigma)



  tmp_df <- tmp_df %>%
    mutate(q10_abundance = q10,
           q90_abundance = q90) %>%
    select(time, everything())

  # Abundance
  g_abundance <- tmp_df %>%
    select(Abundance, time, q10_abundance, q90_abundance) %>%
    ggplot() +
    geom_rect(data = time_rect,
              aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
              fill = alpha("grey", 0.3),
              inherit.aes = FALSE) +
    geom_line(aes(y = Abundance, x = time, colour = "Abundance")) +
    geom_ribbon(aes(y = Abundance, ymin = q10_abundance, ymax = q90_abundance, x=time), fill = alpha(param_col["Abundance"],0.2)) +
    scale_colour_manual(values = param_col) +
    geom_hline(yintercept = op_model$K, linetype = "dashed") +
    labs(subtitle = "Abundance") +
    theme_bw() +
    theme(axis.title.x=element_blank(),
          axis.text.x=element_blank(),
          axis.ticks.x=element_blank(),
          axis.title.y=element_blank(),
          legend.position="none")


  # Depletion
  g_depletion <- tmp_df %>%
    select(Depletion, time) %>%
    ggplot(aes(y = Depletion, x = time, colour = "Depletion")) +
    geom_rect(data = time_rect,
              aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
              fill = alpha("grey", 0.3),
              inherit.aes = FALSE) +
    geom_line() +
    scale_colour_manual(values = param_col) +
    labs(subtitle = "Depletion") +
    theme_bw() +
    theme(axis.title.y=element_blank(),
          legend.position="none")



  # projection
  g_proj <- tmp_df %>%
    filter(time > length_burnin) %>%
    select(time, Abundance, q10_abundance, q90_abundance, survey_estimate) %>%
    ggplot() +
    geom_rect(data = time_rect,
              aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
              fill = alpha("grey", 0.3),
              inherit.aes = FALSE) +
    geom_ribbon(aes(y = Abundance, ymin = q10_abundance, ymax = q90_abundance, x=time), fill = alpha(param_col["Abundance"],0.2)) +
    geom_point(aes(y = survey_estimate, x = time, colour = "survey_estimate"), size = 2, na.rm = TRUE) +
    geom_line(aes(y = Abundance, x = time, colour = "Abundance")) +
    scale_colour_manual(values = param_col) +
    geom_hline(yintercept = op_model$K, linetype = "dashed") +
    labs(subtitle = paste0("Projected abundance<br>",
                           "(<span style='color:",param_col["Abundance"],";'>**real**</span> vs ",
                           "<span style='color:",param_col["survey_estimate"],";'>**survey estimated**</span>)")) +
    theme_bw() +
    theme(plot.subtitle = element_markdown(lineheight = 1.1),
          legend.text = element_markdown(size = 11),
          legend.position="none",
          axis.title.y=element_blank(),
          axis.title.x=element_blank(),
          axis.text.x=element_blank(),
          axis.ticks.x=element_blank())


  # removals
  g_removals <- tmp_df %>%
    filter(time > length_burnin) %>%
    select(time, Removals) %>%
    ggplot() +
    geom_rect(data = time_rect,
              aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
              fill = alpha("grey", 0.3),
              inherit.aes = FALSE) +
    geom_point(aes(y = Removals, x = time, colour = "Removals"), size = 2, na.rm = TRUE) +
    geom_line(aes(y = Removals, x = time, colour = "Removals"), na.rm = TRUE) +
    scale_colour_manual(values = param_col) +
    labs(subtitle = "Removals") +
    theme_bw() +
    theme(axis.title.y=element_blank(),
          legend.position="none")


  # build table
  df_table <- data.frame(
    z = op_model$z,
    MNPL = op_model$MNPL,
    K = op_model$K,
    Rmax = op_model$Rmax,
    CV = op_model$CV,
    CV_env = op_model$CV_env,
    rho = op_model$rho,
    seed_id = op_model$seed_id
    ) %>%
    mutate(across(starts_with("b_"), ~round(.x, digits = 3))) %>%
    reshape(times = names(.),
            v.names = "value",
            direction = "long",
            varying = 1:ncol(.),
            timevar = "param",
            new.row.names = NULL
            ) %>%
    select(-id) %>%
    mutate(value = as.character(value))

  rownames(df_table) <- NULL


  tmp_table <- df_table %>%
    gridExtra::tableGrob(cols = NULL, rows = NULL)

  # assemble all plot with patchwork

  design <- "
  abc
  aef
  "

  if(lower_zero == TRUE) {

    g_long_time <- wrap_elements(tmp_table) +
      g_abundance + ylim(0,NA) +
      g_proj + ylim(0,NA) +
      g_depletion + ylim(0,NA) +
      g_removals + ylim(0,NA) +
      plot_layout(design = design, widths = c(1.0,1.5,1.5))

  } else {

    g_long_time <- wrap_elements(tmp_table) +
      g_abundance +
      g_proj +
      g_depletion +
      g_removals +
      plot_layout(design = design, widths = c(1.0,1.5,1.5))

  }

  return(g_long_time)

}



#' @export
summary_plot.seabird <- function(op_model, lower_zero = FALSE, ...) {

  assert_that(is.list(op_model))
  assert_that(is.logical(lower_zero))
  assert_that(op_model %has_name% c("abundance","removals","survey","depletion","fledgling_rate"))



  param_long <- c("Abundance","Depletion","fledgling_rate","Removals")
  length_tot <- length(op_model$abundance)
  length_burnin <- length_tot - length(op_model$removals)

  # df removals
  df_survey_estimates <- data.frame(
    time =  (length_burnin + 1) : length_tot,
    removals = op_model$removals
  )

  df_survey <- data.frame(
    time = length_burnin + op_model$survey$scans,
    survey_estimate = op_model$survey$mean
  )

  df_survey_estimates <- df_survey_estimates %>%
    left_join(df_survey, by = "time") %>%
    select(survey_estimate, time, removals)


  # build projection background
  time_rect <- data.frame(
    proj_min = length_burnin + 1,
    proj_max = Inf,
    ymin = -Inf,
    ymax = Inf
  )

  tmp_df <- c("abundance","depletion","fledgling_rate") %>%
    lapply(function(x)getElement(op_model,x)) %>%
    do.call(cbind, .) %>%
    as.data.frame() %>%
    mutate(time = 1:n()) %>%
    left_join(df_survey_estimates, by = "time")


  colnames(tmp_df)[c(1:3,ncol(tmp_df))] <- param_long


  # color associated to param
  length_param <- length(c(param_long, "survey_estimate"))
  param_col <- c("#4DAF4A", "#377EB8",
                 # "#FF7F00",
                 "#984EA3","#E41A1C", "#edae49", "#F781BF", "#FFFF33")[1:length_param]
  names(param_col) <- c(param_long, "survey_estimate")


  sigma <- sqrt(log1p(op_model$CV * op_model$CV))
  mu <- log(op_model$abundance) - 0.5 * sigma * sigma
  q10 <- qlnorm(0.1, meanlog = mu, sdlog = sigma)
  q90 <- qlnorm(0.9, meanlog = mu, sdlog = sigma)



  tmp_df <- tmp_df %>%
    mutate(q10_abundance = q10,
           q90_abundance = q90) %>%
    select(time, everything())


  # Modify birthrate value with sex-ratio value (birthrate * 1/proba_female)
  proba_female <- op_model$sexratio[1] / sum(op_model$sexratio)
  # tmp_df <- tmp_df %>%
  #   mutate(Birthrate = Birthrate * 1/proba_female) %>%
  #   mutate(Birthrate = ifelse(Birthrate >= 1, 1, Birthrate))


  # Abundance
  g_abundance <- tmp_df %>%
    select(Abundance, time,
           # q10_abundance, q90_abundance
    ) %>%
    ggplot() +
    geom_rect(data = time_rect,
              aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
              fill = alpha("grey", 0.3),
              inherit.aes = FALSE) +
    geom_line(aes(y = Abundance, x = time, colour = "Abundance")) +
    # geom_ribbon(aes(y = Abundance, ymin = q10_abundance, ymax = q90_abundance, x=time), fill = alpha(param_col["Abundance"],0.2)) +
    scale_colour_manual(values = param_col) +
    geom_hline(yintercept = op_model$K, linetype = "dashed") +
    labs(subtitle = "Abundance") +
    theme_bw() +
    theme(axis.title.x=element_blank(),
          axis.text.x=element_blank(),
          axis.ticks.x=element_blank(),
          axis.title.y=element_blank(),
          legend.position="none")


  # Depletion
  g_depletion <- tmp_df %>%
    select(Depletion, time) %>%
    ggplot(aes(y = Depletion, x = time, colour = "Depletion")) +
    geom_rect(data = time_rect,
              aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
              fill = alpha("grey", 0.3),
              inherit.aes = FALSE) +
    geom_line() +
    scale_colour_manual(values = param_col) +
    labs(subtitle = "Depletion") +
    theme_bw() +
    theme(axis.title.y=element_blank(),
          legend.position="none")


  # fledgling_rate
  g_f_rate <- tmp_df %>%
    select(fledgling_rate, time) %>%
    ggplot(aes(y = fledgling_rate, x = time, colour = "fledgling_rate")) +
    geom_rect(data = time_rect,
              aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
              fill = alpha("grey", 0.3),
              inherit.aes = FALSE) +
    geom_line() +
    scale_colour_manual(values = param_col) +
    labs(subtitle = "Fledgling rate") +
    theme_bw() +
    theme(axis.title.y=element_blank(),
          legend.position="none")

  # # Productivity
  # g_productivity <- tmp_df %>%
  #   select(Productivity,time) %>%
  #   ggplot(aes(y = Productivity, x = time, colour = "Productivity")) +
  #   geom_rect(data = time_rect,
  #             aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
  #             fill = alpha("grey", 0.3),
  #             inherit.aes = FALSE) +
  #   geom_line() +
  #   scale_colour_manual(values = param_col) +
  #   labs(subtitle = "Productivity (female calves)") +
  #   theme_bw() +
  #   theme(axis.title.y=element_blank(),
  #         legend.position="none",
  #         axis.title.x=element_blank(),
  #         axis.text.x=element_blank(),
  #         axis.ticks.x=element_blank())


  # projection
  g_proj <- tmp_df %>%
    filter(time > length_burnin) %>%
    select(time, Abundance,
           # q10_abundance, q90_abundance,
           survey_estimate) %>%
    ggplot() +
    geom_rect(data = time_rect,
              aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
              fill = alpha("grey", 0.3),
              inherit.aes = FALSE) +
    # geom_ribbon(aes(y = Abundance, ymin = q10_abundance, ymax = q90_abundance, x=time), fill = alpha(param_col["Abundance"],0.2)) +
    geom_point(aes(y = survey_estimate, x = time, colour = "survey_estimate"), size = 2, na.rm = TRUE) +
    geom_line(aes(y = Abundance, x = time, colour = "Abundance")) +
    scale_colour_manual(values = param_col) +
    geom_hline(yintercept = op_model$K, linetype = "dashed") +
    labs(subtitle = paste0("Projected abundance<br>",
                           "(<span style='color:",param_col["Abundance"],";'>**real**</span> vs ",
                           "<span style='color:",param_col["survey_estimate"],";'>**survey estimated**</span>)")) +
    theme_bw() +
    theme(plot.subtitle = element_markdown(lineheight = 1.1),
          legend.text = element_markdown(size = 11),
          legend.position="none",
          axis.title.y=element_blank(),
          axis.title.x=element_blank(),
          axis.text.x=element_blank(),
          axis.ticks.x=element_blank())


  # removals
  g_removals <- tmp_df %>%
    filter(time > length_burnin) %>%
    select(time, Removals) %>%
    ggplot() +
    geom_rect(data = time_rect,
              aes(xmin = proj_min, xmax = proj_max, ymin = ymin, ymax = ymax),
              fill = alpha("grey", 0.3),
              inherit.aes = FALSE) +
    geom_point(aes(y = Removals, x = time, colour = "Removals"), size = 2, na.rm = TRUE) +
    geom_line(aes(y = Removals, x = time, colour = "Removals"), na.rm = TRUE) +
    scale_colour_manual(values = param_col) +
    labs(subtitle = "Removals") +
    theme_bw() +
    theme(axis.title.y=element_blank(),
          legend.position="none")


  # build table
  df_table <- data.frame(
    z = op_model$z,
    MNPL = op_model$MNPL,
    K = op_model$K,
    # L = op_model$L,
    MNP = op_model$MNP,
    # b_max = op_model$b_max,
    # b_K = op_model$b_K,
    # CV = op_model$CV,
    CV_env = op_model$CV_env,
    rho = op_model$rho,
    seed_id = op_model$seed_id
  ) %>%
    mutate(across("MNP", ~round(.x, digits = 3))) %>%
    reshape(times = names(.),
            v.names = "value",
            direction = "long",
            varying = 1:ncol(.),
            timevar = "param",
            new.row.names = NULL
    ) %>%
    select(-id) %>%
    mutate(value = as.character(value))

  # Bird param tab
  tab_bird <- data.frame(
    id = 1,
    hatch = op_model$hatch,
    f_max = op_model$f_max,
    f_K = op_model$f_K,
    p_det = op_model$p_det,
    K_ad = op_model$K_ad,
    breed = op_model$breed
  ) %>%
    merge(
      data.frame(
        id = 1:2,
        eggs = op_model$eggs
      ),
      by = "id",
      all = T
    ) %>%
    mutate(across(-id,function(x)as.character(x))) %>%
    t() %>%
    as.data.frame() %>%
    mutate(param = row.names(.)) %>%
    select(param,V1,V2) %>%
    mutate(across(everything(),~ifelse(is.na(.x),"",.x))) %>%
    filter(param != "id")

  rownames(tab_bird) <- NULL

  tab_bird <- tab_bird %>%
    gridExtra::tableGrob(cols = NULL, rows = NULL)


  rownames(df_table) <- NULL

  tmp_table <- df_table %>%
    gridExtra::tableGrob(cols = NULL, rows = NULL)

  # assemble all plot with patchwork

  design <- "
  a#bc
  defg
  "

  if(lower_zero == TRUE) {

    g_long_time <- wrap_elements(tmp_table) +
      # g_productivity + ylim(0,NA) +
      g_abundance + ylim(0,NA) +
      g_proj + ylim(0,NA) +
      wrap_elements(tab_bird) +
      g_f_rate + ylim(0,NA) +
      g_depletion + ylim(0,NA) +
      g_removals + ylim(0,NA) +
      plot_layout(design = design, widths = c(1.0,1.25,1.25,1.25))

  } else {

    g_long_time <- wrap_elements(tmp_table) +
      # g_productivity +
      g_abundance +
      g_proj +
      wrap_elements(tab_bird) +
      g_f_rate +
      g_depletion +
      g_removals +
      plot_layout(design = design, widths = c(1.0,1.25,1.25,1.25))

  }

  return(g_long_time)

}
