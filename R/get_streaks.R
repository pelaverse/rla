#' A function to identify a streak of values above a threshold. Useful to assess the
#' time to reach the conservation objective.
#'
#' @param vec an integer vector of 0 and 1.
#' @param summary logical. Default to TRUE to be used with the function \code{\link{time2CO}}
#'
#' @importFrom assertthat assert_that
#' @importFrom dplyr group_by lag mutate row_number ungroup n
#' @importFrom stats start
#'
#' @return a dataframe
#'
#' @export
#'
#' @seealso \code{\link{time2CO}}
#'
#' @examples
#' get_streaks(vec = c(0, 0, 0, 1, 1, 0, 0, 1))
#' get_streaks(vec = c(0, 0, 0, 1, 1, 0, 0, 1), summary = FALSE)
#'
#' @references
#' Lifted from https://www.r-bloggers.com/2020/06/detecting-streaks-in-r/
get_streaks <- function(vec,
                        summary = TRUE
                        ) {
  ### sanity checks
  assert_that(is.logical(summary))

  ## vec must a vector of 0, 1
  if(all(vec %in% 0:1)) {

    x <- data.frame(trials = vec) %>%
      mutate(lagged = dplyr::lag(trials),
             start = (trials != lagged)
             )

    x[1, "start"] <- TRUE
    x <- x %>%
      mutate(streak_id = cumsum(start))

    if(isTRUE(summary)) {
      x <- x %>%
        group_by(streak_id) %>%
        summarize(value = unique(trials),
                  length = n()
                  )
      x <- x %>%
        # time to the conservation objective
        mutate(time2CO = c(0, cumsum(length)[-nrow(x)])) %>%
        as.data.frame()
    } else {
      x <- x %>%
        group_by(streak_id) %>%
        mutate(streak = row_number()) %>%
        ungroup() %>%
        as.data.frame()
    }
  } else {
    stop("Arg. must include only '0' or '1'")
  }
  return(x)
}
