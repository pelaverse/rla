#' A function to estimate the catch limit with the RLA using Stan.
#'
#' @param op_model a list; the output from one call to function \code{\link{pellatomlinson_dis}}
#' @param rlastan a pre-complied stan model
#' @param bycatch_error_cv a positive scalar; coefficient of variation of error
#' in bycatch estimates. Default to 0.
#' @param n_chains a count; number of chains to initialize. Default to 4 chains.
#' @param n_iter a count; number of iterations of HMC. Default to 2000.
#' @param n_warm a count; number of warm-up iterations. Default to 1000.
#' @param n_thin a count; number of iterations to retain.
#' Default to 1, i.e. keep all iterations.
#' @param convergence a number >= 1 but less than 1.10. The threshold for the
#' Gelman-Brooks-Rubin convergence statistics. Default to 1.025.
#' @param distribution a character string; must be one of "truncnorm",
#' "lognormal", or "gamma" to simulate removals.
#' @param ipl a proportion; the internal protection level below which bycatch is
#' automatically set to 0. Default to 0.54.
#' @param upper a number; the upper bound for the prior on Rmax in the RLA.
#' @param w a positive number; the weight for the RLA likelihood. Default to 1/16.
#' @param z a positive scalar. Shape parameter of assumed Pella-Tomlinson
#' density-dependence function
#' @param B0 a positive scalar. The initial biomass or abundance.
#' @param q a proportion between 0 and 1.
#' @param upper_bound_r a positive scalar. The upper bound for the uniform prior on the
#' maximum growth rate
#' @param upper_bound_phi a positive scalar. The upper bound for the uniform prior on the
#' extraction rate.
#' @param lower_bound_D0 a positive scalar. The lower bound for the uniform prior on the
#' initial depletion.
#' @param memory a character string. Either "short" or "long". Controls weights for the removals'
#' likelihood.
#' @param model a character string. Either "cooke" or "bousquet"
#' @param everything logical; should the stanfit object be returned: if false only
#' the posterior of the catch limit is returned. Default to FALSE.
#'
#' @importFrom rstan sampling stan_model extract Rhat rstan_options
#' @importFrom assertthat assert_that is.number is.count
#' @importFrom parallel detectCores
#'
#' @return a vector with the posterior distribution of the removal rate
#' if parameter convergence has been achieved; NA otherwise.
#' @export
#'
#' @seealso \code{\link{bycatch_obs}}, \code{\link{standata}}
#'
#' @examples
#' \dontrun{
#' library(tidyverse)
#' ## need Stan (Stan is your best friend)
#' library(rstan)
#' options(mc.cores = parallel::detectCores())
#' rstan_options(auto_write = TRUE)
#'
#' ## load Stan models
#' data(rlastan_models)
#' # use uniform priors
#' cat(rlastan_models$uniform)
#' # compile model
#' rlastan <- rstan::stan_model(model_code = rlastan_models$uniform,
#'                              model_name = "RLA"
#'                              )
#' ## load data on the harbour porpoise in the North Sea
#' data(north_sea_hp)
#' op_model <- list(survey = data.frame(mean = north_sea_hp$SCANS$N_hat,
#'                                      cv = north_sea_hp$SCANS$CVs,
#'                                      # indicator variable of when the SCANS
#'                                      # survey took place. 2016 is 51
#'                                      scans = c(29, 40, 51)
#'                                      ),
#'                  removals = north_sea_hp$bycatch$mean
#'                  )
#' # op_model must be of class "pellatomlinson_dis"
#' class(op_model) <- "pellatomlinson_dis"
#'
#' ## fit the RLA
#' mod <- rlafit(op_model = op_model,
#'               rlastan = rlastan,
#'               distribution = "truncnorm",
#'               n_iter = 1.5E4,
#'               n_warm = 5e3,
#'               model = "cooke",
#'               # get the complete stan output
#'               everything = TRUE
#'               )
#'
#' ## check convergence with Rhat
#' plot(mod,
#'      plotfun = 'rhat',
#'      pars = c("removal_limit", "depletion", "K", "r", "abundance")
#'      )
#'
#' ## have a look at the trace of parameters
#' traceplot(mod,
#'           pars = c("depletion", "r"),
#'           inc_warmup = TRUE
#'           )
#'
#' ## have a look at the posterior marginals
#' plot(mod,
#'      plotfun = 'hist',
#'      pars = c("depletion", "r", "removal_limit")
#'      )
#'
#' ## look at the posterior geometry of the two unknown parameters
#' pairs(mod,
#'       pars = c("depletion", "r")
#'       )
#'
#' ## summarizes the posterior
#' print(mod,
#'       pars = c("removal_limit", "depletion", "K", "r", "abundance"),
#'       digits = 3
#'       )
#'
#' ## compute threshold, using the 25% quantile
#' (rstan::extract(mod, "removal_limit")$removal_limit *
#'   last(north_sea_hp$SCANS$N_hat)
#'  ) |>
#'   quantile(probs = 0.25) |>
#'   round()
#'
#' ### Use ART
#' cat(rlastan_models$sspm_trend)
#' # compile model
#' artstan <- rstan::stan_model(model_code = rlastan_models$sspm_trend,
#'                              model_name = "Stochastic SPM with trend (ART2)"
#'                              )
#' # data: Need to assume that abundance in 2015 was the same as in 2016
#' # (time-series of by-catch ends in 2015)
#' op_model <- list(survey = data.frame(mean = north_sea_hp$SCANS$N_hat,
#'                                      cv = north_sea_hp$SCANS$CVs,
#'                                      # indicator variable of when the SCANS
#'                                      # survey took place. 2016 is 51
#'                                      scans = c(29, 40, 50)
#'                                      ),
#'                  removals = north_sea_hp$bycatch$mean
#'                  )
#' # op_model must be of class "pellatomlinson_dis"
#' class(op_model) <- "pellatomlinson_dis"
#' # fit ART
#' mod <- rlafit(op_model = op_model,
#'               rlastan = artstan,
#'               distribution = "truncnorm",
#'               n_iter = 1.5E4,
#'               n_warm = 5e3,
#'               model = "bousquet",
#'               # get the complete stan output
#'               everything = TRUE
#'               )
#' # check convergence with Rhat
#' mod |>
#'  print(pars = c("K", "r", "sigma", "removal_limit"),
#'        digits = 3
#'        )
#' # compute threshold using a recovery factor of 0.2
#' (rstan::extract(mod, "removal_limit")$removal_limit *
#'   last(north_sea_hp$SCANS$N_hat) * 0.2
#'  ) |>
#'    mean() |>
#'    round()
#' }
rlafit <- function(op_model,
                   rlastan,
                   bycatch_error_cv = 0.0,
                   n_chains = 4,
                   n_iter = 2e3,
                   n_warm = 1e3,
                   n_thin = 1,
                   convergence = 1.025,
                   distribution = c("truncnorm", "lognormal", "gamma"),
                   upper = 0.10,
                   w = 1 / 16,
                   ipl = 0.54,
                   z = 2.39,
                   B0 = NULL,
                   q = 1,
                   upper_bound_r = 0.1,
                   upper_bound_phi = 0.1,
                   lower_bound_D0 = 0.3,
                   memory = "short",
                   model = c("cooke", "bousquet"),
                   everything = FALSE
                   ) {

  ### sanity check
  assert_that(class(rlastan) == "stanmodel")
  assert_that(is.list(op_model), (class(op_model) == "pellatomlinson_dis"))
  assert_that(is.numeric(bycatch_error_cv), (bycatch_error_cv >= 0))
  assert_that(is.count(n_chains))
  assert_that(is.count(n_warm), (n_warm >= 200))
  assert_that(is.count(n_iter), (n_iter > n_warm))
  assert_that(is.count(n_thin))
  assert_that(is.number(convergence),
              (convergence <= 1.1),
              (convergence >= 0.99)
              )
  assert_that(is.character(distribution),
              (distribution %in% c("truncnorm", "lognormal", "gamma"))
              )
  assert_that(is.number(ipl), (ipl < 1), (ipl > 0))
  assert_that(is.number(upper), (upper > 0))
  assert_that(is.number(w), (w <= 1), (w > 0))
  assert_that(is.number(z), (z > 0))
  assert_that((is.number(B0) && (B0 > 0)) || is.null(B0))
  assert_that(is.number(q), (q <= 1), (q > 0))
  assert_that(is.number(upper_bound_r), (upper_bound_r > 0))
  assert_that(is.number(upper_bound_phi), (upper_bound_phi > 0))
  assert_that(is.number(lower_bound_D0), (lower_bound_D0 < 1))
  assert_that(length(model) == 1, is.character(model), model %in% c("cooke", "bousquet"))
  assert_that(is.logical(everything))

  ### prepare data
  data4stan <- standata(op_model = op_model,
                        control_rule = cr_param(model = model,
                                                rla = list(upper = upper,
                                                           w = w,
                                                           ipl = ipl
                                                           ),
                                                spm = list(z = z,
                                                           B0 = B0,
                                                           q = q,
                                                           upper_bound_r = upper_bound_r,
                                                           upper_bound_phi = upper_bound_phi,
                                                           lower_bound_D0 = lower_bound_D0,
                                                           memory = memory
                                                           )
                                                )
                        )

  ## add observation error to bycatch
  data4stan$BYCATCH <- bycatch_obs(mu = data4stan$BYCATCH,
                                   cv = bycatch_error_cv,
                                   distribution = distribution
                                   )
  ## which params to monitor
  if(model == "cooke") {
    if(everything) {
      params <- c("removal_limit", "depletion", "K", "r", "abundance")
    } else {
      params <- c("removal_limit", "depletion", "r")
    }
  }
  if(model == "bousquet") {
    if(everything) {
      params <- c("removal_limit", "D0", "K", "r", "sigma", "sigma_max")
    } else {
      params <- c("removal_limit", "r", "sigma")
    }
  }


  # Test if data4stan match rlastan model
  var_data <- extract_data_stan(rlastan)
  if(!all(var_data %in% names(data4stan))) {
    stop("One mustn’t mix dishtowels with napkins")
  }


  ### sampling
  hmc <- sampling(object = rlastan,
                  data = data4stan,
                  pars = params,
                  chains = n_chains,
                  iter = n_iter,
                  warmup = n_warm,
                  thin = n_thin,
                  control = list(adapt_delta = 0.99, max_treedepth = 15)
                  )
  rhat <- apply(as.array(hmc), 3, Rhat)
  gc()
  ### return output if parameters have converged
  if(everything) {
    out <- hmc
  } else {
    if(all(!is.na(rhat)) && all(rhat < convergence)) {
      out <- rstan::extract(hmc, "removal_limit")$removal_limit
    } else {
      out <- rep(NA, n_chains * (n_iter - n_warm) / n_thin)
    }
  }
  return(out)
}
