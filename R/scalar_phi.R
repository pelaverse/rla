#' Find a scalar affecting all age-classes in a matrix population model such that
#' the asymptotic growth rate remains 1 for a given birth rate
#'
#' @param expected_MNP Expected value of MNP to get br_max.
#' @param phi a vector of probabilities; age-specific survival probabilities
#' @param maturity a vector of proportion; age-specific maturity proportions
#' @param logit logical; work on a logit scale? Default to FALSE
#'
#' @importFrom assertthat assert_that is.number
#' @importFrom stats uniroot
#'
#' @return a dataframe with a column birth rate, a column logit and a column scalar
#' @export
#'
#' @seealso \code{\link{inverse_lambda_leslie}}
#'
#' @examples
#' \dontrun{
#' library(tidyverse)
#' # multiplier on phi
#' scalar_phi(expected_MNP = 1.04,
#'            phi = c(0.85, 0.87, rep(0.91, 20), 0.0),
#'            maturity = c(0, 1 / (1 + exp( (4 - 0:21) / 0.5)))
#'            ) %>%
#'   ggplot(aes(x = birth_rate, y = scalar)) +
#'   geom_line() +
#'   theme_bw()
#'
#' # additive effect on logit scale for phi
#' scalar_phi(expected_MNP = 1.04,
#'            phi = c(0.85, 0.87, rep(0.91, 20), 0.0),
#'            maturity = c(0, 1 / (1 + exp( (4 - 0:21) / 0.5))),
#'            logit = TRUE
#'            ) %>%
#'   ggplot(aes(x = birth_rate, y = scalar)) +
#'   geom_line() +
#'   theme_bw()
#' }
scalar_phi <- function(expected_MNP,
                       phi,
                       maturity,
                       logit = FALSE
                       ) {
  ### sanity checks
  assert_that(is.number(expected_MNP), (expected_MNP >= 1))
  assert_that(all(sapply(phi, is.number)), all(phi < 1), all(phi >= 0))
  assert_that(all(sapply(maturity, is.number)),
              all(maturity <= 1),
              all(maturity >= 0)
              )
  assert_that(is.logical(logit))

  ### maximum birth rate
  b_max <- inverse_lambda_leslie(expected_MNP = expected_MNP,
                                 phi = phi,
                                 maturity = maturity
                                 )
  ### birth rate at carrying capacity
  b_K <- inverse_lambda_leslie(expected_MNP = 1.0,
                               phi = phi,
                               maturity = maturity
                               )
  ### birth rate
  br <- seq(b_K, b_max, length.out = 1E2 + 1)
  br <- br[-1]

  get_lambda <- function(scalar, b) {
    # logit transform
    if(logit) {
      new_phi <- plogis(log(phi / (1 - phi)) - scalar)
    } else {
      new_phi <- phi * (1 - scalar)
    }
    A <- Leslie(birth_rate = b,
                phi = new_phi,
                maturity = maturity
                )
    res_lambda <- lambda(M = A)
    return(res_lambda)
  }

  out <- sapply(br, function(b) {
    res <- uniroot(
      function(x) {
        get_lambda(scalar = x, b = b) - 1.0
      },
      lower = ifelse(logit, -10, 1e-4),
      upper = ifelse(logit,  10, 1 - 1e-6),
      tol = 1e-8
    )[1]

    return(as.numeric(res))
  })

  out <- data.frame(birth_rate = br,
                    logit = rep(logit, length(br)),
                    scalar = ifelse(logit, 0, 1) - out
                    )

  return(out)
}
