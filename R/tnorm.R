#' Probability density function of a truncated normal distribution
#'
#' @param x a vector of real values
#' @param location a real scalar: location parameter, default to 0
#' @param scale a positive scalar: scale parameter, default to 1
#' @param left a real scalar: left truncation, default to -Inf
#' @param right a real scalar: right truncation, default to +Inf
#'
#' @importFrom assertthat assert_that is.number is.scalar
#' @importFrom stats dnorm pnorm
#'
#' @return a positive real or vector of positive real values
#' @export
#'
#' @seealso \code{\link{ptnorm}}, \code{\link{qtnorm}}, \code{\link{rtnorm}}
#'
#' @examples
#' ### no truncation
#' dtnorm(x = 2)
#' ### left truncation
#' dtnorm(x = 2, left = 0)
#' ### right truncation
#' dtnorm(x = 2, right = 0)
dtnorm <- function(x, location = 0, scale = 1, left = -Inf, right = +Inf) {
  ### sanity check
  assert_that(is.vector(x) || is.scalar(x))
  assert_that(is.number(location))
  assert_that(is.number(scale), (scale >= 0))
  assert_that(is.scalar(left), is.scalar(right), (left <= right))

  out <- dnorm(x, location, scale) /
    (pnorm(right, location, scale) - pnorm(left, location, scale))

  out[(x < left) | (x > right)] <- 0

  return(out)
}


#' Cumulative density function of a truncated normal distribution
#'
#' @param q a vector of real values (quantiles)
#' @param location a real scalar: location parameter, default to 0
#' @param scale a positive scalar: scale parameter, default to 1
#' @param left a real scalar: left truncation, default to -Inf
#' @param right a real scalar: right truncation, default to +Inf
#'
#' @importFrom assertthat assert_that is.number is.scalar
#' @importFrom stats pnorm
#'
#' @return a positive real or a vector of positive real values between 0 and 1
#' @export
#'
#' @seealso \code{\link{dtnorm}}, \code{\link{qtnorm}}, \code{\link{rtnorm}}
#'
#' @examples
#' ### no truncation
#' ptnorm(q = 2)
#' ### left truncation
#' ptnorm(q = 2, left = 0)
#' ptnorm(q = -1, left = 0)
#' ### right truncation
#' ptnorm(q = -1, right = 0)
#' ptnorm(q = 2, right = 0)
ptnorm <- function(q, location = 0, scale = 1, left = -Inf, right = +Inf) {
  ### sanity check
  assert_that(is.vector(q) || is.scalar(q))
  assert_that(is.number(location))
  assert_that(is.number(scale), (scale >= 0))
  assert_that(is.scalar(left), is.scalar(right), (left <= right))

  out <- (pnorm(q, location, scale) - pnorm(left, location, scale)) /
    (pnorm(right, location, scale) - pnorm(left, location, scale))

  out[q < left] <- 0
  out[q > right] <- 1

  return(out)
}

#' Quantile function of a truncated normal distribution
#'
#' @param p a vector of real values between 0 and 1
#' @param location a real scalar: location parameter, default to 0
#' @param scale a positive scalar: scale parameter, default to 1
#' @param left a real scalar: left truncation, default to -Inf
#' @param right a real scalar: right truncation, default to +Inf
#'
#' @importFrom assertthat assert_that is.number is.scalar
#' @importFrom stats pnorm qnorm
#'
#' @return a real or a vector of real values
#' @export
#'
#' @seealso \code{\link{dtnorm}}, \code{\link{ptnorm}}, \code{\link{rtnorm}}
#'
#' @examples
#' ### no truncation
#' qtnorm(p = 0.5)
#' ### left truncation
#' qtnorm(p = 0.5, left = 0)
#' qtnorm(p = 0.5, left = -1)
#' ### right truncation
#' qtnorm(p = 0.5, right = 0)
#' qtnorm(p = 0.5, right = 1)
qtnorm <- function(p, location = 0, scale = 1, left = -Inf, right = +Inf) {
  ### sanity check
  assert_that((is.vector(p) || is.scalar(p)), (all(p >= 0) && (all(p <= 1))))
  assert_that(is.number(location))
  assert_that(is.number(scale), (scale >= 0))
  assert_that(is.scalar(left), is.scalar(right), (left <= right))

  out <- qnorm((1 - p) * pnorm(left, location, scale) +
                 p * pnorm(right, location, scale),
               location, scale
  )

  return(out)
}


#' Random generator function for a truncated normal distribution
#'
#' @param n a vector of real values between 0 and 1
#' @param location a real scalar: location parameter, default to 0
#' @param scale a positive scalar: scale parameter, default to 1
#' @param left a real scalar: left truncation, default to -Inf
#' @param right a real scalar: right truncation, default to +Inf
#'
#' @importFrom assertthat assert_that is.number is.scalar is.count
#' @importFrom stats runif
#'
#' @return a real or a vector of real values
#' @export
#'
#' @seealso \code{\link{dtnorm}}, \code{\link{ptnorm}}, \code{\link{qtnorm}}
#'
#' @examples
#' ### no truncation
#' rtnorm(n = 10)
#' ### left truncation
#' rtnorm(n = 10, left = 0)
#' rtnorm(n = 10, left = -1)
#' ### right truncation
#' rtnorm(n = 10, right = 0)
#' rtnorm(n = 10, right = 1)
rtnorm <- function(n, location = 0, scale = 1, left = -Inf, right = +Inf) {
  ### sanity check
  assert_that(is.count(n))
  assert_that(is.number(location))
  assert_that(is.number(scale), scale >= 0)
  assert_that(is.scalar(left), is.scalar(right), (left <= right))

  out <- qtnorm(runif(n), location, scale, left, right)

  return(out)
}
