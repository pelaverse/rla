data {
	int<lower = 1> n_survey; // nb of surveys
	int<lower = 1> n_year; // nb of years with removals
  vector<lower = 0.0>[n_survey] SURVEY_mean; // abundance estimates
	vector<lower = 0.0>[n_survey] SURVEY_cv;   // cv of abundance estimates
	vector<lower = 0.0>[n_year] BYCATCH; // bycatch estimates
	real<lower = 0.0> IPL; // Internal Protection Level, same scale as depletion;
  // The downweighting is implemented to reduce variability in bycatch limits (Cooke 1999)
	real<lower = 0.0, upper = 1.0> W; // weight for down-weighting the likelihood as in the IWC CLA
	real<lower = 0.0> UPPER; // upper limit for growth rate
  int<lower = 1, upper = n_year + 1> SCANS[n_survey]; // pointers to years of SCANS survey
}

transformed data {
	vector[n_survey] scale = sqrt(log1p(square(SURVEY_cv)));
	vector[n_survey] location = log(SURVEY_mean) - 0.5 * log1p(square(SURVEY_cv));
}

parameters {
  real u_rho;
	vector[2] upsilon;
}

transformed parameters {
  real rho = tanh(0.75 * u_rho);
	real r = UPPER * inv_logit(1.5 * upsilon[1]); // population growth rate
	real depletion = inv_logit(1.5 * upsilon[2]); // depletion at the end of the time-series
	vector[n_year + 1] abundance;
	real K;
	matrix[2, 2] Omega;
	Omega[1, 1] = 1.0;
	Omega[1, 2] = rho;
	Omega[2, 1] = rho;
	Omega[2, 2] = 1.0;
	// what's observed in the latest survey is a fraction of carrying capacity
	K = SURVEY_mean[n_survey] / depletion;
	// the population begins at carrying capacity
	abundance[1] = K;
	for(t in 1:n_year) {
		abundance[t + 1] = abundance[t] - BYCATCH[t] + r * abundance[t] * (1 - square(abundance[t] / K));
	}
}

model {
  u_rho ~ normal(0.0, 1.0);
	upsilon ~ multi_normal(rep_vector(0.0, 2), Omega);
	for(i in 1:n_survey) {
		target += lognormal_lpdf(abundance[SCANS[i]]| location[i], scale[i]) * W;
	}
}

generated quantities {
	real removal_limit;
	removal_limit = r * fmax(0.0, depletion - IPL);
}