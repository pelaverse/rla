
functions {
  real upper_bound_sigma(real phi, real r, real z) {
    // upper bound for sigma given phi, r and z
    // phi: extraction rate
    // r: growth rate
    // z: shape of the Pella-Tomlinson DD function
    real value;
    value = (1 + inv(z)) * log1p(z); // numerator
    value += -log(z) - log1m(phi + r * (z + 1) * inv(z)); // denominator

    return sqrt(expm1(value));
  }

  real removals_lpdf(vector y, int n, real phi, real r, real z, real sigma, real q, real D0, real B0, vector w) {
    // log likelihood for removals
    // y: data
    // n: length of time series for y
    // phi: extraction rate
    // r: growth rate
    // z: shape of the Pella-Tomlinson DD function
    // sigma: environmental stochasticity
    // q: fraction
    // D0: initial depletion
    // B0: initial abundance
    // weights for likelihood
    real coef = exp(log(D0) - log(q) - log(phi) - log(B0));
    real value = 0.0;
    // currently does not include the first datum
    for(t in 2:n) {
      real mu = log(y[t-1] + ((z + 1) * r / z) * y[t-1] * (1 - pow(coef * y[t-1], z)) - phi * y[t-1]) - 0.5 * square(sigma);
      value += w[t] * lognormal_lpdf(y[t] | mu, sigma);
    }

    return value;
  }

  real abundance_lpdf(vector y, real phi, real q, real w) {
    // log likelihood for abundance/biomass given removals
    // y: vector of length 3, with 1-removals, 2-estimated abundance/biomass, and
    //    3-associated coefficient of variation
    // phi: extraction rate
    // q: fraction
    real tau = sqrt(log1p(y[3] * y[3])); // scale parameter of log-normal distribution
    real mu = log(y[1]) - log(q) - log(phi) - 0.5 * square(tau);  // location parameter of log-normal distribution

    return w * lognormal_lpdf(y[2] | mu, tau);
  }
}

data {
  int<lower = 2> n_year;
  int<lower = 1> n_survey;
  vector<lower = 0.0>[n_year] BYCATCH;     // removals time series
  vector<lower = 0.0>[n_year] Wr;          // weights for likelihood (removals time series)
  vector<lower = 0.0>[3] SURVEY[n_survey]; // abundance/biomass
  vector<lower = 0.0>[n_survey] Ws;        // weights for likelihood (survey time series)
  real<lower = 0.0> z;                     // shape of the Pella-Tomlinson DD function
  real<lower = 0.0> B0;                    // B0: initial abundance/biomass
  real<lower = 0.0, upper = 1.0> q;        // fraction
  real<lower = 0.0> upper_bound_r;         // upper bound for r
  real<lower = 0.0> upper_bound_phi;       // upper bound for phi
  real<lower = 0.0> lower_bound_D0;        // lower bound for D0, the initial depletion
}

transformed data {
  real nu = 1; // cauchy
  real prior_scale_slope = 0.05455187;     // see Cook et al. skeptical prior
  vector[n_survey] prop;
  vector[n_survey] survey_t;
  for(t in 1:n_survey) {
    prop[t] = log(SURVEY[t, 2]) - log(SURVEY[1, 2]);
    survey_t[t] = (t - 1) * 1.0 / (n_survey - 1);
  }
}

parameters {
  real<lower = 0.0, upper = 1.0> unscaled_r;    // growth rate
  real<lower = 0.0, upper = 1.0> unscaled_phi;  // extraction rate
  real<lower = 0.0, upper = 1.0> unscaled_sigma;// environmental stochasticity
  real<lower = lower_bound_D0, upper = 1.0> D0; // D0: initial depletion
  real aux_slope;                               // normal deviate
  real<lower = 0.0> scale_sq;                   // inverse gamma for scale mixture
  real<lower = 0.0, upper = 10> sigma_res;      // residual scale
}

transformed parameters {
  real r = unscaled_r * upper_bound_r;        // growth rate
  real phi = unscaled_phi * upper_bound_phi;  // extraction rate
  real sigma_max = upper_bound_sigma(phi, r, z);
  real sigma = unscaled_sigma * sigma_max;    // environmental stochasticity
  real slope = prior_scale_slope * aux_slope * sqrt(scale_sq); // cauchy
}

model {
  // priors
  aux_slope ~ normal(0.0, 1.0);               // normal auxiliary variate
  scale_sq ~ inv_gamma(0.5 * nu, 0.5 * nu);   // scale mixture (cauchy with nu = 1)
  unscaled_r ~ uniform(0.0, 1.0);
  unscaled_phi ~ uniform(0.0, 1.0);
  unscaled_sigma ~ uniform(0.0, 1.0);
  // weighted likelihoods
  // removals
  target += removals_lpdf(BYCATCH| n_year, phi, r, z, sigma, q, D0, B0, Wr);
  // surveys
  for(i in 1:n_survey) {
    target += abundance_lpdf(SURVEY[i]| phi, q, Ws[i]);
  }
  // trend in abundance
  target += normal_lpdf(prop| slope * survey_t, sigma_res);
}

generated quantities{
  real removal_limit = phi * fmin(1.0, exp(slope));
  real K = B0 * inv(D0);
}
