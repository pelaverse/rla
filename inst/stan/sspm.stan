
// add weights in the likelihoods
functions {
  real upper_bound_sigma(real phi, real r, real z) {
    // upper bound for sigma given phi, r and z
    // phi: extraction rate
    // r: growth rate
    // z: shape of the Pella-Tomlinson DD function
    real value;
    // sanity checks
    if (!(phi >= 0)) {
     reject("upper_bound_sigma(phi, r, z): phi must be positive; found phi = ", phi);
    }
    if (!(r >= 0)) {
     reject("upper_bound_sigma(phi, r, z): r must be positive; found r = ", r);
    }
    if (!(z >= 0)) {
     reject("upper_bound_sigma(phi, r, z): z must be positive; found z = ", z);
    }
    value = (1 + inv(z)) * log1p(z); // numerator
    value += -log(z) - log1m(phi + r * (z + 1) * inv(z)); // denominator
    return sqrt(expm1(value));
  }
// check how they did it in the stan depository
  real removals_lpdf(vector y, int n, real phi, real r, real z, real sigma, real q, real D0, real B0, vector w) {
    // log likelihood for removals
    // y: data
    // n: length of time series for y
    // phi: extraction rate
    // r: growth rate
    // z: shape of the Pella-Tomlinson DD function
    // sigma: environmental stochasticity
    // q: fraction
    // D0: initial depletion
    // B0: initial abundance
    // weights for likelihood
    real coef;
    real value = 0.0;
    // sanity checks
    if (!(n >= 2)) {
     reject("removals_lpdf(y, phi, r, z, sigma, q, D0, B0): y must be of size greater than one; found size = ", n);
    }
    if (!(phi >= 0)) {
     reject("removals_lpdf(y, w, phi, r, z, sigma, q, D0, B0): phi must be positive; found phi = ", phi);
    }
    if (!(r >= 0)) {
     reject("removals_lpdf(y, phi, r, z, sigma, q, D0, B0): r must be positive; found r = ", r);
    }
    if (!(z >= 0)) {
     reject("removals_lpdf(y, phi, r, z, sigma, q, D0, B0): z must be positive; found z = ", z);
    }
    if (!(sigma >= 0)) {
     reject("removals_lpdf(y, phi, r, z, sigma, q, D0, B0): sigma must be positive; found sigma = ", sigma);
    }
    if (!(q >= 0)) {
     reject("removals_lpdf(y, phi, r, z, sigma, q, D0, B0): q must be positive; found q = ", q);
    }
    if (!(q <= 1)) {
     reject("removals_lpdf(y, phi, r, z, sigma, q, D0, B0): q must be less than or equal to one; found q = ", q);
    }
    if (!(D0 >= 0)) {
     reject("removals_lpdf(y, phi, r, z, sigma, q, D0, B0): D0 must be positive; found D0 = ", D0);
    }
    if (!(B0 >= 0)) {
     reject("removals_lpdf(y, phi, r, z, sigma, q, D0, B0): B0 must be positive; found B0 = ", B0);
    }
    coef = exp(log(D0) - log(q) - log(phi) - log(B0));
    // currently does not include the first datum
    for(t in 2:n) {
      real mu = log(y[t-1] + ((z + 1) * r / z) * y[t-1] * (1 - pow(coef * y[t-1], z)) - phi * y[t-1]) - 0.5 * square(sigma);
      value += w[t] * lognormal_lpdf(y[t] | mu, sigma);
    }
    return value;
  }

  real abundance_lpdf(vector y, real phi, real q, real w) {
    // log likelihood for abundance/biomass given removals
    // y: vector of length 3, with 1-removals, 2-estimated abundance/biomass, and
    //    3-associated coefficient of variation
    // phi: extraction rate
    // q: fraction
    real tau; // scale parameter of log-normal distribution
    real mu;  // location parameter of log-normal distribution

    // sanity checks
    if (!(phi >= 0)) {
     reject("abundance_lpdf(y, phi, q): phi must be positive; found phi = ", phi);
    }
    if (!(q >= 0)) {
     reject("abundance_lpdf(y, phi, q): q must be positive; found q = ", q);
    }
    if (!(q <= 1)) {
     reject("abundance_lpdf(y, phi, q): q must be less than or equal to one; found q = ", q);
    }
    tau = sqrt(log1p(y[3] * y[3]));
    mu = log(y[1]) - log(q) - log(phi) - 0.5 * square(tau);
    return w * lognormal_lpdf(y[2] | mu, tau);
  }
}

data {
  int<lower = 2> n_year;
  int<lower = 1> n_survey;
  vector<lower = 0.0>[n_year] BYCATCH;     // removals time series
  vector<lower = 0.0>[n_year] Wr;          // weights for likelihood (removals time series)
  vector<lower = 0.0>[3] SURVEY[n_survey]; // abundance/biomass
  vector<lower = 0.0>[n_survey] Ws;        // weights for likelihood (survey time series)
  real<lower = 0.0> z;                     // shape of the Pella-Tomlinson DD function
  real<lower = 0.0> B0;                    // B0: initial abundance/biomass
  real<lower = 0.0, upper = 1.0> q;        // fraction
  real<lower = 0.0> upper_bound_r;         // upper bound for r
  real<lower = 0.0> upper_bound_phi;       // upper bound for phi
  real<lower = 0.0> lower_bound_D0;        // lower bound for D0, the initial depletion
}

parameters {
  real<lower = 0.0, upper = 1.0> unscaled_r;    // growth rate
  real<lower = 0.0, upper = 1.0> unscaled_phi;  // extraction rate
  real<lower = 0.0, upper = 1.0> unscaled_sigma;// environmental stochasticity
  real<lower = lower_bound_D0, upper = 1.0> D0; // D0: initial depletion
}

transformed parameters {
  real r = unscaled_r * upper_bound_r;        // growth rate
  real phi = unscaled_phi * upper_bound_phi;  // extraction rate
  real sigma_max = upper_bound_sigma(phi, r, z);
  real sigma = unscaled_sigma * sigma_max;    // environmental stochasticity
}

model {
  // priors
  unscaled_r ~ uniform(0.0, 1.0);
  unscaled_phi ~ uniform(0.0, 1.0);
  unscaled_sigma ~ uniform(0.0, 1.0);
  // weighted likelihoods
  // removals
  target += removals_lpdf(BYCATCH| n_year, phi, r, z, sigma, q, D0, B0, Wr);
  // surveys
  for(i in 1:n_survey) {
  	target += abundance_lpdf(SURVEY[i]| phi, q, Ws[i]);
  }
}

generated quantities{
  // take into account misspecification: if sigma is too large,
  // what to use for sigma_min: log(1.2) ie 20% environmental stochasticity for example
  // real removal_limit = pow(phi, 1 + fmax(0, sigma - environmental_stochasticity))
  // real removal_limit = pow(phi, fmin(1, exp(sigma)));
  real removal_limit = phi; // use this one if a structural relationship is enforced with the upper boudn for sigma
  real K = B0 * inv(D0);
}
