library(RLA)

library("tidyverse")
### load data for the harbour porpoise in the North Sea
data("north_sea_hp")
data("scenarios_north_sea_hp")
### use the first scenario
dyn <- pellatomlinson_rla(MNPL = scenarios_north_sea_hp$MNPL[1],
                          K = north_sea_hp$life_history$K,
                          L = north_sea_hp$life_history$L,
                          eta = north_sea_hp$life_history$eta,
                          phi = north_sea_hp$life_history$phi,
                          m = north_sea_hp$life_history$maturity,
                          MNP = scenarios_north_sea_hp$MNP[1],
                          # series of catches
                          catches = bycatch_rw(n = 51,
                                               K = north_sea_hp$life_history$K,
                                               rate = scenarios_north_sea_hp$rate[1],
                                               cv = scenarios_north_sea_hp$cv_byc[1],
                                               seed_id = scenarios_north_sea_hp$seed[1]
                                               ),
                          # expected coefficient of variation for the survey estimates
                          CV = scenarios_north_sea_hp$cv_obs[1],
                          # CV of environmental stochasticity on birth rate
                          CV_env = scenarios_north_sea_hp$cv_env[1],
                          # scans survey
                          scans = c(29, 40, 51),
                          verbose = TRUE,
                          everything = TRUE,
                          seed_id = scenarios_north_sea_hp$seed[1]
                          )
library(rstan)
options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)

## load Stan models
data(rlastan_models)
# use uniform priors: have a look at the Stan code
cat(rlastan_models$uniform)
# compile model
rlastan <- rstan::stan_model(model_code = rlastan_models$uniform,
                             model_name = "RLA"
                             )

rla <- forward_rla(rlalist = dyn,
                   rlastan = rlastan,
                   bycatch_variation_cv = 0.3,
                   bycatch_error_cv = c(0.3, 0),
                   q = 0.9,
                   distribution = "truncnorm",
                   # upper bound for the prior on r, the population growth rate
                   upper = 0.1,
                   bias_abund = 2,
                   bias_byc = 2,
                   catastrophe = 0.6
                   )

rla$management

rla$depletion

rlalist <- list(survey = data.frame(mean = north_sea_hp$SCANS$N_hat,
                                    cv = north_sea_hp$SCANS$CVs,
                                    # indicator variable of when the SCANS
                                    # survey took place. 2016 is 51
                                    scans = c(29, 40, 51)
                                    ),
                removals = north_sea_hp$bycatch$mean
                )

plot(hp$abundance[-length(hp$abundance)], hp$abundance[-1] / hp$abundance[-length(hp$abundance)],
     xlab = "Population size", ylab = "Growth rate",
     pch = 20, las = 1, bty = 'n'
     )
points(hp2$abundance[-length(hp2$abundance)],
       hp2$abundance[-1] / hp2$abundance[-length(hp2$abundance)],
       pch = 21
       )
