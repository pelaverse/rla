test_that("pbr_nouveau works", {
  data("north_sea_hp")
  data("scenarios_north_sea_hp")
  ### check class
  expect_error(pbr_nouveau(op_model = rlastan,
                           distribution = "truncnorm",
                           q = 0.5,
                           F_r = 0.5
                           )
               )
  dyn <- pellatomlinson_dis(
    vital_param = vital_param_dis(
      MNPL = scenarios_north_sea_hp$MNPL[1],
      K = north_sea_hp$life_history$K,
      L = north_sea_hp$life_history$L,
      eta = north_sea_hp$life_history$eta,
      phi = north_sea_hp$life_history$phi,
      m = north_sea_hp$life_history$maturity,
      MNP = scenarios_north_sea_hp$MNP[1]
    ),
    # series of removals
    removals = bycatch_rw(n = 51,
                         K = north_sea_hp$life_history$K,
                         rate = scenarios_north_sea_hp$rate[1],
                         cv = scenarios_north_sea_hp$cv_byc[1],
                         seed_id = scenarios_north_sea_hp$seed[1]
                         ),
    # expected coefficient of variation for the survey estimates
    CV = scenarios_north_sea_hp$cv_obs[1],
    # CV of environmental stochasticity on birth rate
    CV_env = scenarios_north_sea_hp$cv_env[1],
    # scans survey
    scans = c(29, 40, 51),
    verbose = FALSE,
    everything = TRUE,
    seed_id = scenarios_north_sea_hp$seed[1]
  )

  ## call the pbr_nouveau management procedure
  res <- pbr_nouveau(op_model = dyn,
                     distribution = "truncnorm",
                     q = 0.5,
                     F_r = 0.5
                     )
  expect_equal(class(res), "list")
  expect_equal(length(res), 2)
  expect_equal(class(res$management), class(res$depletion))
})

test_that("pbr_nouveau is somewhat foolproof", {
  data("north_sea_hp")
  data("scenarios_north_sea_hp")
  dyn <- pellatomlinson_dis(
    vital_param = vital_param_dis(MNPL = scenarios_north_sea_hp$MNPL[1],
                                  K = north_sea_hp$life_history$K,
                                  L = north_sea_hp$life_history$L,
                                  eta = north_sea_hp$life_history$eta,
                                  phi = north_sea_hp$life_history$phi,
                                  m = north_sea_hp$life_history$maturity,
                                  MNP = scenarios_north_sea_hp$MNP[1]
                                  ),

    # series of removals
    removals = bycatch_rw(n = 51,
                         K = north_sea_hp$life_history$K,
                         rate = scenarios_north_sea_hp$rate[1],
                         cv = scenarios_north_sea_hp$cv_byc[1],
                         seed_id = scenarios_north_sea_hp$seed[1]
                         ),
    # expected coefficient of variation for the survey estimates
    CV = scenarios_north_sea_hp$cv_obs[1],
    # CV of environmental stochasticity on birth rate
    CV_env = scenarios_north_sea_hp$cv_env[1],
    # scans survey
    scans = c(29, 40, 51),
    verbose = FALSE,
    everything = TRUE,
    seed_id = scenarios_north_sea_hp$seed[1]
  )

  expect_error(pbr_nouveau(op_model = NA, distribution = "truncnorm"))
  expect_error(pbr_nouveau(op_model = 1, distribution = "truncnorm"))
  expect_error(pbr_nouveau(op_model = dyn, bycatch_variation_cv = NA, distribution = "truncnorm"))
  expect_error(pbr_nouveau(op_model = dyn, bycatch_variation_cv = -0.2, distribution = "truncnorm"))
  expect_error(pbr_nouveau(op_model = dyn, bycatch_variation_cv = c(0.2, 0.1), distribution = "truncnorm"))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "whatever"))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", frequency = 0))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", frequency = c(6, 4)))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", horizon = 0))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", horizon = NA))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", horizon = c(100, 200)))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", Ktrend = 0))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", Ktrend = NA))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", Ktrend = -1))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", Ktrend = c(1, 2)))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", bias = 0))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", bias = NA))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", bias = c(1, 2)))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", catastrophe = 1.1))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", catastrophe = NA))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", catastrophe = c(0.1, 0.2)))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", Rmax = c(0.1, 0.04)))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", Rmax = NA))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", F_r = c(0.1, 0.5)))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", F_r = 1.1))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", q = c(0.1, 0.5)))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", q = 1.1))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", q = -0.2))
  expect_error(pbr_nouveau(op_model = dyn, distribution = "truncnorm", random = "NA"))
})
