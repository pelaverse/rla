test_that("depensation works", {
  expect_equal(round(depensation(decay = 1, x = 0.5), 1), 0.4)
  expect_equal(depensation(decay = +Inf, x = 0.5), 1)
})

test_that("decay must be non-nil", {
  expect_error(depensation(decay = 0, x = 0.5))
})

test_that("x must be positive", {
  expect_error(depensation(decay = +Inf, x = -1))
})
