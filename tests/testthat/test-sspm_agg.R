test_that("sspm_agg works", {
  hp <- sspm_agg(vital_param = vital_param_agg(depletion0 = 0.3,
                                               Rmax = 0.04
                                               ),
                 n_removals = 100,
                 CV_env = 0.1,
                 seed_id = 20230416
                 )
  expect_equal(sum(is.na(hp$removals)), 0)
  expect_equal(length(hp$removals), 100)
  expect_equal(length(hp), 14)
})

test_that("sspm_agg fails", {
  expect_error(sspm_agg(vital_param = vital_param_agg(depletion0 = 0.0,
                                                      Rmax = 0.04
                                                      ),
                        n_removals = 100,
                        CV_env = 0.1,
                        seed_id = 20230416
                        )
               )
  expect_error(sspm_agg(vital_param = vital_param_agg(depletion0 = 0.3,
                                                      MNPL = 0.0,
                                                      Rmax = 0.04
                                                      ),
                        n_removals = 100,
                        CV_env = 0.1,
                        seed_id = 20230416
                        )
               )
  expect_error(sspm_agg(vital_param = vital_param_agg(depletion0 = 0.3,
                                                      Rmax = -0.04
                                                      ),
                        n_removals = 100,
                        CV_env = 0.1,
                        seed_id = 20230416
                        )
               )
  expect_error(sspm_agg(vital_param = vital_param_agg(depletion0 = 0.3,
                                                      Rmax = 0.04,
                                                      K = 0
                                                      ),
                        n_removals = 100,
                        CV_env = 0.1,
                        seed_id = 20230416
                        )
               )
  expect_error(sspm_agg(vital_param = vital_param_agg(depletion0 = 0.3,
                                                      Rmax = 0.04
                                                      ),
                        n_removals = 0,
                        CV_env = 0.1,
                        seed_id = 20230416
                        )
               )
  expect_error(sspm_agg(vital_param = vital_param_agg(depletion0 = 0.3,
                                                      Rmax = 0.04
                                                      ),
                        n_removals = 100,
                        CV_env = 0.0,
                        seed_id = 20230416
                        )
               )
  expect_error(sspm_agg(vital_param = vital_param_agg(depletion0 = 0.3,
                                                      Rmax = 0.04
                                                      ),
                        n_removals = 100,
                        CV_env = 0.1,
                        seed_id = "20230416"
                        )
               )
  expect_error(sspm_agg(vital_param = vital_param_agg(depletion0 = 0.3,
                                                      Rmax = 0.04
                                                      ),
                        n_removals = 100,
                        CV_env = 0.1,
                        seed_id = 20230416,
                        random = "TRUE"
                        )
               )
})
