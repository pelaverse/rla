test_that("qtlnorm fails", {
  expect_error(qtlnorm(-1))
  expect_error(qtlnorm(0, location = 1:2))
  expect_error(qtlnorm(0, scale = 1:2))
  expect_error(qtlnorm(0, left = c(0.1, 0.5)))
  expect_error(qtlnorm(0, right = 1:2))
  expect_error(qtlnorm(0, left = 1, right = 0.5))
})

test_that("qtlnorm works", {
  expect_equal(qtlnorm(p = 0.5), 1)
  expect_equal(round(qtlnorm(p = 0.5, left = 0.5), 6), 1.364626)
  expect_equal(round(qtlnorm(p = 0.5, right = 1), 6), 0.509416)
  expect_equal(round(qtlnorm(p = 0.5, left = 0.5, right = 1), 6), 0.721504)
})
