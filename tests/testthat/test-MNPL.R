test_that("Type of output and length", {

  # unique z
  expect_equal(MNPL(z = 5.05), 0.7001595, tolerance = 1e5)
  expect_equal(length(MNPL(z = 5.05)), 1)

  # vector of z
  expect_equal(MNPL(z = c(33.65, 11.22)), c(0.9000022, 0.8000425), tolerance = 1e5)
  expect_equal(length(MNPL(z = c(33.65, 11.22))), 2)
})


test_that("Expected errors with MNPL", {

  # unique z
  expect_error(MNPL(z = "5.05"), class = "error")

  # vector of z
  expect_error(MNPL(z = c(5.05,"fites")), class = "error")

  # list of z
  expect_error(MNPL(z = list(5.05,11.22)), class = "error")
})
