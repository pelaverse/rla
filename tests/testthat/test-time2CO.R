test_that("time2CO works", {
  expect_equal(time2CO(vec = c(0, 0, 0, 1, 1, 0, 0, rep(1, 7))), 7)
  expect_equal(time2CO(vec = c(0, 0, 0, 1, 1, 0, 0, rep(1, 6))), NA)
})

test_that("threshold must be a non-nil integer", {
  expect_error(time2CO(vec = c(0, 0, 0, 1, 1, 0, 0, rep(1, 7)), threshold = 0))
  expect_error(time2CO(vec = c(0, 0, 0, 1, 1, 0, 0, rep(1, 7)), threshold = -1))
})

test_that("vec must be a vector of 0s and 1s only", {
  expect_error(time2CO(vec = c(-1, 0, 0, 1, 1, 0, 0, rep(1, 7))))
})
