test_that("forward_rla fails", {
  library("tidyverse")
  ### load data for the harbour porpoise in the North Sea
  data("north_sea_hp")
  data("scenarios_north_sea_hp")
  ### trick
  rlastan <- list(a = 1)
  class(rlastan) <- "stanmodel"
  ### check class
  expect_error(forward_rla(op_model = rlastan,
                           rlastan = rlastan,
                           q = 0.5,
                           distribution = "truncnorm",
                           upper = 0.1
                           )
               )
  ### use the first scenario
  dyn <- pellatomlinson_dis(
    vital_param = vital_param_dis(
      MNPL = scenarios_north_sea_hp$MNPL[1],
      K = north_sea_hp$life_history$K,
      L = north_sea_hp$life_history$L,
      eta = north_sea_hp$life_history$eta,
      phi = north_sea_hp$life_history$phi,
      m = north_sea_hp$life_history$maturity,
      MNP = scenarios_north_sea_hp$MNP[1]
    ),
    # series of removals
    removals = bycatch_rw(n = 51,
                         K = north_sea_hp$life_history$K,
                         rate = scenarios_north_sea_hp$rate[1],
                         cv = scenarios_north_sea_hp$cv_byc[1],
                         seed_id = scenarios_north_sea_hp$seed[1]
                         ),
    # expected coefficient of variation for the survey estimates
    CV = scenarios_north_sea_hp$cv_obs[1],
    # CV of environmental stochasticity on birth rate
    CV_env = scenarios_north_sea_hp$cv_env[1],
    # scans survey
    scans = c(29, 40, 51),
    verbose = FALSE,
    everything = TRUE,
    seed_id = scenarios_north_sea_hp$seed[1]
  )
  ###
  expect_error(forward_rla(op_model = NULL,
                           rlastan = rlastan,
                           q = 0.5,
                           distribution = "truncnorm",
                           upper = 0.1
                           )
               )
  expect_error(forward_rla(op_model = dyn,
                           rlastan = NULL,
                           q = 0.5,
                           distribution = "truncnorm",
                           upper = 0.1
                           )
               )
  expect_error(forward_rla(op_model = dyn,
                           rlastan = rlastan,
                           q = 0,
                           distribution = "truncnorm",
                           upper = 0.1
                           )
               )
  expect_error(forward_rla(op_model = dyn,
                           rlastan = rlastan,
                           q = 0.5,
                           distribution = NULL,
                           upper = 0.1
                           )
               )
  expect_error(forward_rla(op_model = dyn,
                           rlastan = rlastan,
                           q = 0.5,
                           distribution = "truncnorm",
                           upper = 0.0
                           )
               )
  expect_error(forward_rla(op_model = dyn,
                           rlastan = rlastan,
                           q = 0.5,
                           distribution = "truncnorm",
                           upper = 0.1,
                           random = NULL
                           )
               )
  expect_error(forward_rla(op_model = dyn,
                           rlastan = rlastan,
                           q = 0.5,
                           distribution = "truncnorm",
                           upper = 0.1,
                           bycatch_variation_cv = -1
                           )
               )
  expect_error(forward_rla(op_model = dyn,
                           rlastan = rlastan,
                           q = 0.5,
                           distribution = "truncnorm",
                           upper = 0.1,
                           bycatch_error_cv = c(0, 0, 0)
                           )
               )
  expect_error(forward_rla(op_model = dyn,
                           rlastan = rlastan,
                           q = 0.5,
                           distribution = "truncnorm",
                           upper = 0.1,
                           frequency = 0
                           )
               )
  expect_error(forward_rla(op_model = dyn,
                           rlastan = rlastan,
                           q = 0.5,
                           distribution = "truncnorm",
                           upper = 0.1,
                           horizon = 0
                           )
               )
  expect_error(forward_rla(op_model = dyn,
                           rlastan = rlastan,
                           q = 0.5,
                           distribution = "truncnorm",
                           upper = 0.1,
                           bias_abund = -1
                           )
               )
  expect_error(forward_rla(op_model = dyn,
                           rlastan = rlastan,
                           q = 0.5,
                           distribution = "truncnorm",
                           upper = 0.1,
                           bias_byc = -1
                           )
               )
  expect_error(forward_rla(op_model = dyn,
                           rlastan = rlastan,
                           q = 0.5,
                           distribution = "truncnorm",
                           upper = 0.1,
                           Ktrend = 0
                           )
               )
  expect_error(forward_rla(op_model = dyn,
                           rlastan = rlastan,
                           q = 0.5,
                           distribution = "truncnorm",
                           upper = 0.1,
                           catastrophe = 1.1
                           )
               )
})
